FROM maven:3.8.6-amazoncorretto-11

# RUN addgroup -S spring && adduser -S spring -G spring
# USER spring:spring
ARG JAR_FILE=target/*.jar

WORKDIR /app

COPY . .

# RUN mvn clean install
RUN mvn clean package

EXPOSE 8080

# ENTRYPOINT ["java","-jar","/app/target/mvn-test.jar"]
ENTRYPOINT ["mvn", "spring-boot:run"]
